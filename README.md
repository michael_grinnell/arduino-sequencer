# L.E.D. Sequencer

L.E.D. Sequencer is a project developed in c++, using an Arduino, the FastLED library and the Juce audio framework 

The project is a prototype sequencer/sampler that maps audio to visual using a grid Light Emitting Diodes (LEDs). This grid consists of seventy-two LEDs, made up of six rows, and twelve columns. Each row on the grid holds one sample, and each instance of that sample is represented by an illuminated LED. 

This prototype allows users to record and playback audio samples in a sequence dictated by the placement of lit LEDs throughout the grid. When a user records a sample, they can then place this sample on the grid by rotating two rotary encoders. When the user presses play, the LEDs move from left to right and trigger the corresponding sample when it reaches the end of the grid. The user can also change the tempo of the system via a rotary encoder.

## Hardware
The hardware is wired as follows

 
![alt text](http://michaelgrinnellgamesdesign.com/img/Led_Diagram.png "LED Sequencer Wiring Diagram")


### 1.	Play Button
The Play button is connected to data pin 9. When pressed it will toggle the system between the playing and stopped states.
### 2.	Record Button
The Record button is connected to data pin 8. When it is pressed, the system will enter the recording state and begin recording input from the default system microphone. When it is released the recording will stop and the system will enter the placing sound state.
### 3.	Row Encoder
The row encoders pins are connected to 5, 2, 7 for the CLK, DT and SW respectively. If the system is in the playing state, turning the row encoder to the left will slow down the playback speed of the system. Turning it to the right will increase the playback speed. If the system is in the placing sound state turning the row encoder will allow the user to move the sound up and down between rows.
### 4.	Column Encoder
The Column encoder pins are connected to 6,3 and 4 for the CLK, DT and SW respectively. If the system is not in the placing sound state, clicking the button of the rotary encoder will put the system into the placing sound state. Once in the placing sound state turning the column encoder allows the user to select the column to place the sound in. Clicking it again will then place a sound in that position. If there is already a sound in that position clicking it will remove the sound from that position.
### 5.	LED Grid
The LEDs are supplied by a separate 12v power supply with a 100-Microfarad capacitor across the live and ground. There is a 330 Ohm resistor placed across the data pin which is connected to pin 13.


[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"
## Usage

The Arduino software package should be installed on the Arduino that is connected to a windows machine. It is important that it is a windows machine as the software for reading serial ports will not run correctly on other operating systems. The machine it is connected to must also have a microphone and speakers connected, the system will connect using the default devices.

