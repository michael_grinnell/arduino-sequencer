#pragma once
#include <JuceHeader.h>
#include "Enums.h"


struct DebugInfo : public Message
{
    DebugInfo(String s) : info(s) {}

    String info;
};

struct SerialMessage : public Message
{
    SerialMessage(Enums::SerialCommand sc) : command(sc) {}

    //Returns the command of a error if the message is not a SerialMessage.
    static Enums::SerialCommand getCommand(const Message& message)
    {
        if (auto* sm = dynamic_cast<const SerialMessage*> (&message))
            return sm->command;

        return  Enums::SerialCommand::Error;
    }

    static unsigned char getTrack(const Message& message)
    {
        if (auto* sm = dynamic_cast<const SerialMessage*> (&message))
            return sm->track;

        return  255;
    }

    Enums::SerialCommand command;

    unsigned char track;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SerialMessage)
};