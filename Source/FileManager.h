#pragma once
#include <JuceHeader.h>

//This is responsible for the managment of audio recordings
// it can
//- Save A File
//- Delete A File
//- Handle when to overwrite a file

class FileManager
{
public:
	FileManager();
	~FileManager();

	void setFileCount();

	std::unique_ptr<AudioFormatReaderSource> loadFile(int i);

	void saveFile();
	void deleteFile();

	File handleNewFile();

	std::unique_ptr<File> getRandomTrack();

	File ReplaceTrack(int trackNum, File newFile);

	bool ReplaceRandomTrack();

	int getFileCount();

	int getCurrentFile();

	String getDirectory();

private:
	AudioFormatManager formatManager;

	String directory;

	int maxTracks;
	int fileCount;
	int currentFile;
};