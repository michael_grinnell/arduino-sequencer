#include "CommunicationManager.h"
#include "MainComponent.h"
#include "SerialMessage.h"

CommunicationManager::CommunicationManager(MainComponent& m)
    : arduino("\\\\.\\COM3"),
    Thread(String("CommunicationManagerThread")),
    main(m) {
    startThread();
    setConnectionStatus();
}

CommunicationManager::~CommunicationManager() {
    stopThread(2000);
    // Close Serial Connection
}

void CommunicationManager::setConnectionStatus() {
    bool connected = arduino.isConnected();

    mConnectionStatus = connected ? "Connected" : "Not Connected";

    if (!connected) {
        main.postMessage(new SerialMessage(Enums::SerialCommand::Error));
    }
}

String CommunicationManager::getDubugInfo() {
    return "DEBUG INFO\n"
        "Connection status: " +
        mConnectionStatus +
        "\n"
        "Serial Command Recieved: " +
        mSerialData + "\n";
}

// This function just runs a seperate thread that is constantly listening to the
// serial port for messages recived from the ardiuno
void CommunicationManager::run() {
    // DBG("RUNNING!");
    while (!threadShouldExit()) {
        char output[MAX_DATA_LENGTH];

        // Set all the data of the array to 0, so we know when to exit any
        // processing loops
        char* begin = output;
        char* end = begin + sizeof(output);
        std::fill(begin, end, 0);

        // check if there is any data to process
        int status = arduino.readSerialPort(output, MAX_DATA_LENGTH);

        if (status != 0) {
            // if the output is 1-6 send a play command
            if (output[0] > 0 && output[0] < Enums::SerialCommand::Play) {
                int i = 0;
                // We need to loop through the array until we get a 0,
                // This accounts for recieving multiple play commands at the same time
                while (output[i]) {
                    // DBG("Play Command");
                    auto m = new SerialMessage(Enums::SerialCommand::Play);
                    m->track = (output[i]) - 1;
                    main.postMessage(m);
                    i++;
                }
            }
            else if (output[0] == Enums::SerialCommand::StopPlaying) {
                // mSerialData = "Released";
                // do nothing
            }
            else if (output[0] == Enums::SerialCommand::Record) {
                // mSerialData = "Record";
                main.postMessage(new SerialMessage(Enums::SerialCommand::Record));
            }
            else if (output[0] == Enums::SerialCommand::StopRecording) {
                // mSerialData = "Stop Record";
                main.postMessage(
                    new SerialMessage(Enums::SerialCommand::StopRecording));
            }
            else if (output[0] == Enums::SerialCommand::SetTrack) {
                auto m = new SerialMessage(Enums::SerialCommand::SetTrack);
                m->track = (output[1]);

                //  mSerialData = "Set Track number" + String(m->track);
                main.postMessage(m);
            }
            // main.postMessage(new DebugInfo(getDubugInfo()));
        }
    }
}
