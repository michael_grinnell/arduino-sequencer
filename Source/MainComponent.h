#pragma once

#include <JuceHeader.h>
#include "Enums.h"
#include "AudioRecorder.h"
#include "CommunicationManager.h"
#include "SerialMessage.h"
#include "FileManager.h"

class MainComponent   : 
    public AudioAppComponent,
    public ChangeListener,
    private Timer,
    public MessageListener
{
public:

#pragma region Initialization

    MainComponent();
    ~MainComponent();

    void requestPermissions();
    void initializeAudio();

    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;

    void loadAudio(int i);


#pragma endregion

#pragma region Getters/Setters

#pragma endregion

#pragma region Processing

    void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;
    void processAudio(const AudioSourceChannelInfo& bufferToFill);
    void paint(Graphics& g) override;


#pragma endregion

#pragma region Updates

    void changeState(Enums::TransportState newState, int source);

#pragma endregion


#pragma region Events

    void resized() override;
    void timerCallback() override;
    void changeListenerCallback(ChangeBroadcaster* source) override;

    void startRecording();
    void stopRecording();

    void handleMessage(const Message& message) override;

    void DisplayAlert();

#pragma endregion

private:
    CommunicationManager mCommunicationManager;
    FileManager fileManager;

    OwnedArray<AudioTransportSource> transportSource;
    OwnedArray<AudioFormatReaderSource> readerSource;
    MixerAudioSource mixerSource;

    std::vector<Enums::TransportState> state;

    AudioDeviceManager audioDeviceManager;
    AudioRecorder recorder;

    String mDebug;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
