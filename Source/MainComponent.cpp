#include "MainComponent.h"
#include "Constants.h"

#pragma region Initialization

MainComponent::MainComponent() :
    mCommunicationManager(*this)
{
    setSize(800, 600);
    requestPermissions();
    initializeAudio();
    startTimer(30);
    audioDeviceManager.addAudioCallback(&recorder);
}

MainComponent::~MainComponent()
{
    // This shuts down the audio device and clears the audio source.
    shutdownAudio();
    audioDeviceManager.removeAudioCallback(&recorder);
}

void MainComponent::requestPermissions()
{

    // Some platforms require permissions to open input channels so request that here
    if (RuntimePermissions::isRequired(RuntimePermissions::recordAudio)
        && !RuntimePermissions::isGranted(RuntimePermissions::recordAudio))
    {
        RuntimePermissions::request(RuntimePermissions::recordAudio,
            [&](bool granted) { if (granted)  setAudioChannels(2, 2); });
    }
    else
    {
        // Specify the number of input and output channels that we want to open
        setAudioChannels(2, 2);
    }

    RuntimePermissions::request(RuntimePermissions::recordAudio,
        [this](bool granted)
        {
            int numInputChannels = granted ? 2 : 0;
            audioDeviceManager.initialise(numInputChannels, 2, nullptr, true, {}, nullptr);
        });
}

void MainComponent::initializeAudio()
{
    for (size_t i = 0; i < MAX_SOUNDS; ++i)
    {
        transportSource[i]->addChangeListener(this);
        loadAudio(i);
    }    
}

void MainComponent::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    transportSource.clear();
    mixerSource.removeAllInputs();

    for (size_t i = 0; i < MAX_SOUNDS; i++)
    {
        transportSource.add(std::unique_ptr<AudioTransportSource>(new AudioTransportSource()));
        transportSource[i]->prepareToPlay(samplesPerBlockExpected, sampleRate);
        mixerSource.addInputSource(transportSource[i], true);
        state.push_back(Enums::TransportState::Stopped);
    }   
}

void MainComponent::releaseResources()
{
    for (size_t i = 0; i < MAX_SOUNDS; i++)
        transportSource[i]->releaseResources();
}

void MainComponent::loadAudio(int i)
{
    //Loads the audio from a wav file into the audioTranspot source
        std::unique_ptr<AudioFormatReaderSource> source = fileManager.loadFile(i);

        if (source != nullptr)
        {
            transportSource[i]->setSource(source.get(), 0, nullptr, 44100);
            readerSource.add(std::unique_ptr<AudioFormatReaderSource>(source.release()));
        }
}

#pragma endregion

#pragma region Processing

void MainComponent::getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill)
{
    processAudio(bufferToFill);
}

void MainComponent::processAudio(const AudioSourceChannelInfo& bufferToFill)
{
    if (readerSource.isEmpty())
    {
        bufferToFill.clearActiveBufferRegion();
        return;
    }

    mixerSource.getNextAudioBlock(bufferToFill);
}

void MainComponent::paint(Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
    g.setColour(Colours::green);
    g.drawFittedText(mDebug,0,0,400,400,Justification::topLeft,10,1);
}

#pragma endregion

#pragma region Updates

void MainComponent::changeState(Enums::TransportState newState, int source)
{
    if (state[source] != newState)
    {
        state[source] = newState;

        switch (state[source])
        {
        case Enums::TransportState::Stopped:
            transportSource[source]->setPosition(0.0);
            break;

        case Enums::TransportState::Starting:
            transportSource[source]->setPosition(0.0);
            transportSource[source]->start();
            break;

        case Enums::TransportState::Stopping:
            transportSource[source]->stop();
            transportSource[source]->setPosition(0.0);
            break;
        }
    }
}

#pragma endregion

#pragma region Events

void MainComponent::resized(){}

void MainComponent::timerCallback()
{
    repaint();
}

void MainComponent::changeListenerCallback(ChangeBroadcaster* source)
{
    for (size_t i = 0; i < MAX_SOUNDS; ++i)
    {
        if (source == transportSource[i])
        {
            if (transportSource[i]->isPlaying())
                changeState(Enums::TransportState::Playing,i);
            else
                changeState(Enums::TransportState::Stopped,i);
        }
    }

}

void MainComponent::startRecording()
{
    //DBG("RECORDING");
    if (!RuntimePermissions::isGranted(RuntimePermissions::writeExternalStorage))
    {
        //DBG("Requesting permissions");
        SafePointer<MainComponent> safeThis(this);

        RuntimePermissions::request(RuntimePermissions::writeExternalStorage,
            [safeThis](bool granted) mutable
            {
                if (granted)
                    safeThis->startRecording();
            });
        return;
    }

    File temp = fileManager.handleNewFile();

    recorder.startRecording(temp);
}

void MainComponent::stopRecording()
{
    recorder.stop(); 
}

// This function is responsible for processing messages recieved from the communication manager
void MainComponent::handleMessage(const Message& message)
{
    if (auto* debug = dynamic_cast<const DebugInfo*> (&message))
    {
        mDebug = debug->info;
    }
    else
    {
        auto command = SerialMessage::getCommand(message);
        auto sourceTrack = SerialMessage::getTrack(message);

        if (sourceTrack == 255)
            return;

        switch (command)
        {
        case Enums::Error:
            DisplayAlert();
            break;
        case Enums::Play:
            changeState(Enums::TransportState::Starting, sourceTrack);
            break;
        case Enums::StopPlaying:
            changeState(Enums::TransportState::Stopping, sourceTrack);
            break;
        case Enums::Record:
            startRecording();
            break;
        case Enums::StopRecording:
            stopRecording();
            break;

        case Enums::SetTrack:
            fileManager.ReplaceTrack(sourceTrack, File(fileManager.getDirectory() +"temp" + ".wav"));        
            loadAudio(sourceTrack);

            if (fileManager.ReplaceRandomTrack())
            {
                loadAudio(fileManager.getCurrentFile());
            }

            break;
        }
    }



}

void MainComponent::DisplayAlert()
{
    AlertWindow alert("ERROR", "THERE WAS A PROBLEM CONNECTING TO THE DEVICE", AlertWindow::AlertIconType::WarningIcon, this);
    alert.addButton("OK", 0);
    alert.runModalLoop();
}



#pragma endregion





