#pragma once

namespace Enums
{
    enum TransportState
    {
        Stopped,
        Starting,
        Playing,
        Stopping
    };

    enum SerialCommand
    {
        Error = 0,
        Play = 7,
        StopPlaying,
        Record,
        StopRecording,
        SetTrack
    };

}
