#include "FileManager.h"
#include "Constants.h"
#include <stdlib.h>
#include <time.h>

FileManager::FileManager()
    : directory(File::getCurrentWorkingDirectory().getFullPathName() +
        "\\Audio\\Audio_"),
    maxTracks(6),
    fileCount(0),
    currentFile(0) {
    formatManager.registerBasicFormats();
    setFileCount();
    srand(time(NULL));
}

FileManager::~FileManager() {}

//Counts the files in the audio directory
void FileManager::setFileCount() {
    bool found = true;

    while (found) {
        File file(directory + String(fileCount) + ".wav");

        if (file.exists()) {
            fileCount++;
        }
        else {
            found = false;
        }
    }
}

//loads a file from a particular index
std::unique_ptr<AudioFormatReaderSource> FileManager::loadFile(int i) {
    File file(directory + String(i) + ".wav");

    auto* reader = formatManager.createReaderFor(file);

    if (reader != nullptr) {
        std::unique_ptr<AudioFormatReaderSource> newSource(
            new AudioFormatReaderSource(reader, true));
        currentFile = i;

        return newSource;
    }

    return nullptr;
}

void FileManager::saveFile() {}

void FileManager::deleteFile() {}

File FileManager::handleNewFile() {
    // Create a temp file to stroe the audio
    // this file only replaces a track if the user decides to place the recording
    File file(directory + "temp" + ".wav");

    if (file.exists()) {
        file.deleteFile();
    }

    return file;
}

bool FileManager::ReplaceRandomTrack() {
    // if we hit the probability of replacing a random track
    if (rand() % 100 < RANDOM_PROBABILITY) {
        // if we find a valid random
        if (auto newTrack = getRandomTrack()) {
            // find a valid index to replace
            int index = rand() % maxTracks;
            if (index == currentFile) {
                index--;
                if (index < 0) {
                    index += 2;
                }
            }
            ReplaceTrack(index, *newTrack);
            return true;
        }
    }
    return false;
}

std::unique_ptr<File> FileManager::getRandomTrack() {
    // We cant replace anything if there arent enough recordings
    if (fileCount > maxTracks) {
        // get a random index greater than the number of tracks
        // but less than the total file count
        int fileIndex = (rand() % (fileCount - maxTracks)) + maxTracks;
        File randomTrack(directory + String(fileIndex) + ".wav");

        if (randomTrack.exists()) {
            return std::make_unique<File>(randomTrack);
        }
    }
    return nullptr;
}

File FileManager::ReplaceTrack(int trackNum, File newFile) {
    File oldFile(directory + String(trackNum) + ".wav");

    if (oldFile.exists()) {
        fileCount++;
        oldFile.moveFileTo(directory + String(fileCount) + ".wav");
    }

    newFile.moveFileTo(directory + String(trackNum) + ".wav");
    currentFile = trackNum;

    return newFile;
}

int FileManager::getFileCount() {
    return fileCount;
}

int FileManager::getCurrentFile() {
    return currentFile;
}

String FileManager::getDirectory() {
    return directory;
}