#include "SerialPort.h"
#include <JuceHeader.h>

SerialPort::SerialPort(char *portName)
{
    //We're not yet connected
    this->connected = false;

    //Try to connect to the given port through CreateFile
    this->handler = CreateFileA(static_cast<LPCSTR>(portName),
                                GENERIC_READ | GENERIC_WRITE,
                                0,
                                NULL,
                                OPEN_EXISTING,
                                FILE_ATTRIBUTE_NORMAL,
                                NULL);

    //Check if the connection was successfull
    if (this->handler == INVALID_HANDLE_VALUE){
        //If not success full display an Error
        if (GetLastError() == ERROR_FILE_NOT_FOUND){
            DBG("ERROR: Handle was not attached. Reason: port " << portName << " not available");
        }
    else
        {
            DBG("ERROR!!!");
        }
    }
    else {
        //If connected we try to set the comm parameters
        DCB dcbSerialParameters = {0};

        //Try to get the current
        if (!GetCommState(this->handler, &dcbSerialParameters)) {
            DBG("failed to get current serial parameters");
        }
        else {
            //Define serial connection parameters for the arduino board
            dcbSerialParameters.BaudRate = CBR_9600;
            dcbSerialParameters.ByteSize = 8;
            dcbSerialParameters.StopBits = ONESTOPBIT;
            dcbSerialParameters.Parity = NOPARITY;
            dcbSerialParameters.fDtrControl = DTR_CONTROL_ENABLE;

            //Set the parameters and check for their proper application
            if (!SetCommState(handler, &dcbSerialParameters))
            {
                DBG("ALERT: could not set Serial port parameters\n");
            }
            else {
                //If everything went fine we're connected
                this->connected = true;
                //Flush any remaining characters in the buffers 
                PurgeComm(this->handler, PURGE_RXCLEAR | PURGE_TXCLEAR);
                //We wait 2s as the arduino board will be reseting
                Sleep(ARDUINO_WAIT_TIME);
            }
        }
    }
}

SerialPort::~SerialPort()
{
    if (this->connected){
        this->connected = false;
        CloseHandle(this->handler);
    }
}

int SerialPort::readSerialPort(char *buffer, unsigned int buf_size)
{
    //Number of bytes we'll have read
    DWORD bytesRead;
    //Number of bytes we'll really ask to read
    unsigned int toRead = 0;

    //Use the ClearCommError function to get status info on the Serial port
    ClearCommError(this->handler, &this->errors, &this->status);

    //Check if there is something to read
    if (this->status.cbInQue > 0){

        //If there is we check if there is enough data to read the required number
        //of characters, if not we'll read only the available characters to prevent
        //locking of the application.
        if (this->status.cbInQue > buf_size){
            toRead = buf_size;
        }
        else toRead = this->status.cbInQue;
    }

    // Try to read the require number of chars, and return the number of read bytes on success
    if (ReadFile(this->handler, buffer, toRead, &bytesRead, NULL)) return bytesRead;

    //If nothing has been read, or that an error was detected return 0
    return 0;
}

bool SerialPort::writeSerialPort(char *buffer, unsigned int buf_size)
{
    DWORD bytesSend;

    if (!WriteFile(this->handler, (void*) buffer, buf_size, &bytesSend, 0)){
        ClearCommError(this->handler, &this->errors, &this->status);
        return false;
    }
    else return true;
}

bool SerialPort::isConnected()
{
    return this->connected;
}