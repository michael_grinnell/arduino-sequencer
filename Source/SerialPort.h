#pragma once
//This Class has been taken from the ardiuno website
//https://playground.arduino.cc/Interfacing/CPPWindows/

#define ARDUINO_WAIT_TIME 2000
#define MAX_DATA_LENGTH 255

#include <windows.h>

class SerialPort
{
private:
    //Serial comm handler
    HANDLE handler;
    //Connection status
    bool connected;
    //Get various information about the connection
    COMSTAT status;
    //Keep track of last error
    DWORD errors;

public:
    //Initialize Serial communication with the given COM port
    SerialPort(char *portName);
    //Close the connection
    ~SerialPort();

    //Read data in a buffer, if nbChar is greater than the
    //maximum number of bytes available, it will return only the
    //bytes available. The function return -1 when nothing could
    //be read, the number of bytes actually read.
    int readSerialPort(char *buffer, unsigned int buf_size);

    //Writes data from a buffer through the Serial connection
    //return true on success.
    bool writeSerialPort(char *buffer, unsigned int buf_size);

    //Check if we are actually connected
    bool isConnected();
};

