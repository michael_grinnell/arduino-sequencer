#pragma once

#include <JuceHeader.h>
#include"SerialPort.h"
#include "Enums.h"
#include "AudioRecorder.h"
#include "CommunicationManager.h"
#include "SerialMessage.h"

class MainComponent;

// This Class is responsible for recieving input from the serail port and sending the information on to be processed
class CommunicationManager : 
	public Thread
{
public:
	CommunicationManager(MainComponent& m);
	~CommunicationManager();

	void setConnectionStatus();

	String getDubugInfo();

private:
	void run() override;

private:
	MainComponent& main;
	SerialPort arduino;
	String mSerialData{ "Empty" };
	String mConnectionStatus{"Not Connected"};
};