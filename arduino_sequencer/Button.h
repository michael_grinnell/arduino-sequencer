#ifndef BUTTON_H
#define BUTTON_H
#include <Arduino.h>

class Button {
 private:
  byte pin;
  byte state;
  byte lastState;
  byte commandOn;
  byte commandOff;

  bool pressed;
  bool released;

 public:
  Button(byte pin, byte commandOn, byte commandOff) {
    pressed = false;
    released = false;
    this->pin = pin;
    this->commandOn = commandOn;
    this->commandOff = commandOff;
    state = LOW;
    lastState = LOW;
    pinMode(pin, INPUT);
  }

  void update() {
    state = digitalRead(pin);

    if (state != lastState) {
      sendCommand();
    }
    lastState = state;
  }

  bool wasPressed() {
    if (pressed) {
      pressed = false;
      return true;
    }
    return false;
  }

  bool wasReleased() {
    if (released) {
      released = false;
      return true;
    }
    return false;
  }

  void sendCommand() {
    if (state == HIGH) {
      pressed = true;
      // Serial.write(commandOn);
    } else {
      released = true;
      //  Serial.write(commandOff);
    }
  }
};

#endif
