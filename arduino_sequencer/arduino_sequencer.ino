#include "Constants.h"
#include "Button.h"
#include "Encoder.h"
#include "LEDManager.h"

Button playButton(BUTTON_PLAY_PIN, COMMAND_PLAY, COMMAND_STOP);
Button recButton(BUTTON_REC_PIN, COMMAND_RECORD, COMMAND_RECORD_STOP);

Encoder* colEncoder;
Encoder* rowEncoder;

LEDManager ledManager(NUM_LEDS, NUM_ROWS);

void setup() {
  Serial.begin(9600);

  colEncoder = new Encoder(COL_ENCODER_CLK, COL_ENCODER_DT, COL_ENCODER_SW);
  rowEncoder = new Encoder(ROW_ENCODER_CLK, ROW_ENCODER_DT, ROW_ENCODER_SW);
}

void loop() {
  playButton.update();

  if (playButton.wasPressed()) {
    handlePlayPress();
  }

  recButton.update();

  if (recButton.wasPressed()) {
    handleRecPress();
  } else if (recButton.wasReleased()) {
    handleRecRelease();
  }

  colEncoder->update();
  handleColTurn();

  rowEncoder->update();
  handleRowTurn();

  ledManager.update();
}

void handlePlayPress() {
 // Serial.println("Play Press");
  if (ledManager.getState() == State::Playing) {
    ledManager.setState(State::Stopped);
  } else {
    ledManager.setState(State::Playing);
  }
}

void handleRecPress() { 
 // Serial.println("rec Press");
  ledManager.setState(State::Recording); }

void handleRecRelease() { 
  //Serial.println("rec release");
  ledManager.setState(State::PlacingSound); }

void handleRowTurn() {
  if (ledManager.getState() == State::PlacingSound) {
    if (rowEncoder->wasTurnedRight()) {
     // Serial.println("Row right");
      ledManager.moveLED(true);
    } else if (rowEncoder->wasTurnedLeft()) {
      //Serial.println("row left");
      ledManager.moveLED(false);
    }

    if (rowEncoder->wasPressed()) {
     // Serial.println("row Press");
      ledManager.setSound();
    }
  }

  if (ledManager.getState() == State::Playing ||
      ledManager.getState() == State::Stopped) {
    if (rowEncoder->wasPressed()) {
      //Serial.println("row Press");
      ledManager.setState(State::PlacingSound);
    }
  }
}

void handleColTurn() {
  if (ledManager.getState() == State::PlacingSound) {
    if (colEncoder->wasTurnedRight()) {
      //Serial.println("col right");
      ledManager.moveRow(true);
    } else if (colEncoder->wasTurnedLeft()) {
      //Serial.println("col left");
      ledManager.moveRow(false);
    }
  } else if (ledManager.getState() == State::Playing) {
    if (colEncoder->wasTurnedRight()) {
      //Serial.println("col right");
      ledManager.changeSpeed(false);
    } else if (colEncoder->wasTurnedLeft()) {
      //Serial.println("col left");
      ledManager.changeSpeed(true);
    }
  }
}
