#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <Arduino.h>

//LEDS
const byte LED_PIN = 13;
const byte NUM_LEDS =72;
const byte NUM_LEDS_PER_ROW  =12;
const byte NUM_ROWS =6;

//Button PINS
const byte BUTTON_PLAY_PIN = 9;
const byte BUTTON_REC_PIN = 8;

//Encoder PINS
const byte COL_ENCODER_CLK = 6;
const byte COL_ENCODER_DT = 3;
const byte COL_ENCODER_SW = 4;

const byte ROW_ENCODER_CLK = 5;
const byte ROW_ENCODER_DT = 2;
const byte ROW_ENCODER_SW = 7;

// Serial Commmands
// command play, anything under that number means play that track
// e.g. 5 = play track 5, 2 = play track 2
const byte COMMAND_PLAY = 7;
const byte COMMAND_STOP = 8;
const byte COMMAND_RECORD = 9;
const byte COMMAND_RECORD_STOP = 10;
const byte COMMAND_NEW_SOUND = 11;

//BPM TIMES
const int MIN_SPEED = 3000; //20bpm
const int MAX_SPEED = 100; //600bpm

#endif
