#ifndef ENCODER_H
#define ENCODER_H
#include <Arduino.h>

class Encoder {
 private:
  byte CLK;
  byte DT;
  byte SW;

  byte lastCLK;
  byte lastDT;
  byte lastSW;

  int pos;
  int lastPos;

  bool turnedLeft;
  bool turnedRight;
  bool pressed;
  bool released;

  uint8_t prevNextCode = 0;
  uint16_t store = 0;

 public:
  Encoder(byte CLK, byte DT, byte SW) {
    this->CLK = CLK;
    this->DT = DT;
    this->SW = SW;

    lastCLK = 0;
    lastDT = 0;
    lastSW = 0;

    pos = 0;
    lastPos = 0;

    pinMode(CLK, INPUT);
    pinMode(DT, INPUT);
    pinMode(SW, INPUT);
  }

  byte getInterruptPin() { return DT; }

  void update() {
    int currentSW = digitalRead(SW);

    if (currentSW != lastSW) {
      if (currentSW == 1) {
        released = true;
        pressed = false;
      } else {
        pressed = true;
        released = false;
      }
      lastSW = currentSW;
    }

    if (pos > lastPos) {
      turnedRight = false;
      turnedLeft = true;
    } else if (pos < lastPos) {
      turnedLeft = false;
      turnedRight = true;
    }

    lastPos = pos;

    static int8_t val;

    if (val = read_rotary()) {
      if (prevNextCode == 0x0b) {
        pos++;
      }

      if (prevNextCode == 0x07) {
        pos--;
      }
    }
  }

//Thanks to John Main for the robust rotary encoder read code
//https://www.best-microcontroller-projects.com/rotary-encoder.html

  int8_t read_rotary() {
    static int8_t rot_enc_table[] = {0, 1, 1, 0, 1, 0, 0, 1,
                                     1, 0, 0, 1, 0, 1, 1, 0};

    prevNextCode <<= 2;
    if (digitalRead(DT)) prevNextCode |= 0x02;
    if (digitalRead(CLK)) prevNextCode |= 0x01;
    prevNextCode &= 0x0f;

    // If valid then store as 16 bit data.
    if (rot_enc_table[prevNextCode]) {
      store <<= 4;
      store |= prevNextCode;
      // if (store==0xd42b) return 1;
      // if (store==0xe817) return -1;
      if ((store & 0xff) == 0x2b) return -1;
      if ((store & 0xff) == 0x17) return 1;
    }
    return 0;
  }

  bool wasTurnedLeft() {
    if (turnedLeft) {
      turnedLeft = false;
      return true;
    }
    return false;
  }

  bool wasTurnedRight() {
    if (turnedRight) {
      turnedRight = false;
      return true;
    }
    return false;
  }

  bool wasPressed() {
    if (pressed) {
      pressed = false;
      return true;
    }
    return false;
  }

  bool wasReleased() {
    if (released) {
      released = false;
      return true;
    }
    return false;
  }
};

#endif
