#ifndef UTILITY_H
#define UTILITY_H

namespace Utility {

// reverses the value of a number in a given range
int reverseRange(int num, int min, int max) { return (max + min) - num; }

// wraps a number around between a given min and max
int wrap(int min, int max, int num) {
  if (num > max) {
    return min;
  } else if (num < min) {
    return max;
  }
  return num;
}
}

#endif
