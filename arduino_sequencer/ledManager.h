#ifndef LED_MANAGER_H
#define LED_MANAGER_H
#include <Arduino.h>
#include <FastLED.h>
#include "Timer.h"
#include "Constants.h"
#include "Utility.h"

enum State { Playing, Stopped, Recording, PlacingSound };

class LEDManager {
 private:
  State state;
  State lastState;
  CRGB leds[NUM_LEDS];
  byte ledPositions[NUM_LEDS];

  byte placingSoundIndex;
  byte placingSoundRow;
  byte recordingIndex;

  byte totalLeds;
  byte rows;
  byte ledsPerRow;

  Timer blinkTimer;
  Timer recTimer;
  bool lightsOn;
  bool movePos;
  bool newSound;

  int playbackSpeed;

 public:
  LEDManager(int numLeds, int numRows)
      : totalLeds(numLeds),
        rows(numRows),
        ledsPerRow(totalLeds / rows),
        placingSoundIndex(0),
        placingSoundRow(0),
        recordingIndex(0),
        state(State::Stopped),
        lastState(State::Stopped),
        playbackSpeed(500),
        blinkTimer(playbackSpeed / 2),
        recTimer(375),
        lightsOn(true),
        newSound(false) {
    FastLED.addLeds<WS2811, LED_PIN, BRG>(leds, NUM_LEDS);
    FastLED.setBrightness(50);
    FastLED.clear();

    for (int i = 0; i < NUM_LEDS; i++) {
      ledPositions[i] = 255;
    }

    // we can set any leds we like to come on by default by assigning them the
    // value of their index
    // ledPositions[11] = 11;
    // ledPositions[13] = 13;
    // ledPositions[33] = 33;
    // ledPositions[39] = 39;
    // ledPositions[55] = 55;
    // ledPositions[65] = 65;
    blinkTimer.reset();
  };

  //__________________STATE UPDATES______________________\\

  void update() {
    if (lastState != state) {
      handleStateChange();
    }
    switch (state) {
      case Playing:
        updatePlaying();
        break;
      case Stopped:
        updateStopped();
        break;
      case Recording:
        updateRecording();
        break;
      case PlacingSound:
        updatePlacingSound();
        break;
      default:
        break;
    }
  };

  void updatePlaying() {
    if (blinkTimer.isFinished()) {
      lightsOn = !lightsOn;
      blinkTimer.reset();

      if (lightsOn) {
        updatePositions();
      }
    }

    FastLED.clear();

    if (lightsOn) {
      setLightsOn();
    }

    FastLED.show();
  };

  void updateStopped() {
    setLightsOn();
    FastLED.show();
  };

  void updateRecording() {
    if (recordingIndex >= NUM_LEDS) {
      return;
    }

    if (blinkTimer.isFinished()) {
      leds[recordingIndex] = CRGB::Red;
      recordingIndex++;
      blinkTimer.reset();
    }

    FastLED.show();
  };

  void updatePlacingSound() {
    FastLED.clear();
    setLightsOn();

    if (blinkTimer.isFinished()) {
      lightsOn = !lightsOn;
      blinkTimer.reset();
    }

    if (lightsOn) {
      leds[placingSoundIndex] = CRGB::Red;
    } else {
      leds[placingSoundIndex] = CRGB::Black;
    }

    FastLED.show();

    // Turn on leds
    // Flash red the one your poitioning
  };

  void handleStateChange() {
    FastLED.clear();
    FastLED.show();

    recordingIndex = 0;
    placingSoundIndex = 0;
    placingSoundRow = 0;

    if (newSound) {
      newSound = false;
    }

    if (lastState == State::Recording) {
      Serial.write(COMMAND_RECORD_STOP);
      newSound = true;
    }

    lastState = state;

    switch (state) {
      case Playing:
        blinkTimer.set(playbackSpeed / 2);
        blinkTimer.reset();
        break;
      case Stopped:

        break;
      case Recording:
        Serial.write(COMMAND_RECORD);
        blinkTimer.set(15);
        recTimer.reset();
        break;
      case PlacingSound:
        blinkTimer.set(400);
        blinkTimer.reset();

        break;
      default:
        break;
    }
  }

  void setLightsOn() {
    for (int i = 0; i < NUM_LEDS; i++) {
      if (ledPositions[i] != 255) {
        if (i / 12 == 0) {
          leds[ledPositions[i]] = CRGB(0, 127, 255);  // ROW 1
        } else if (i / 12 == 1) {
          leds[ledPositions[i]] = CRGB(0, 255, 55);  // ROW 2
        } else if (i / 12 == 2) {
          leds[ledPositions[i]] = CRGB(255, 255, 0);  // ROW 3
        } else if (i / 12 == 3) {
          leds[ledPositions[i]] = CRGB(255, 127, 0);  // ROW 4
        } else if (i / 12 == 4) {
          leds[ledPositions[i]] = CRGB(255, 0, 255);  // ROW 5
        } else if (i / 12 == 5) {
          leds[ledPositions[i]] = CRGB(119, 0, 255);  // ROW 6
        }
      }
    }
  }

  //__________________POSITION UPDATES______________________\\

  void updatePositions() {
    int endPos = ledsPerRow - 1;
    bool forward = true;

    for (int i = 0; i < rows; i++) {
      loop(i * ledsPerRow, endPos, forward);

      endPos += ledsPerRow;
      forward = !forward;
    }
  }

  void loop(int start, int end, bool increment) {
    if (increment) {
      for (int i = start; i <= end; i++) {
        updateLedPosition(i, 1, start, end);
      }
    } else {
      for (int i = end; i >= start; i--) {
        updateLedPosition(i, -1, start, end);
      }
    }
  }

  void updateLedPosition(int index, int increment, int min, int max) {
    if (ledPositions[index] != 255) {
      ledPositions[index] += increment;

      if (ledPositions[index] > max) {
        // Serial.write(COMMAND_PLAY);
        Serial.write(ledPositions[index] / 12);
        ledPositions[index] = min;
      } else if (ledPositions[index] < min) {
        ledPositions[index] = max;
        // Serial.write(COMMAND_PLAY);
        Serial.write((ledPositions[index] + 12) / 12);
      }
    }
  }

  //__________________MOVE EVENTS______________________\\

  void moveLED(bool right) {
    if (placingSoundRow % 2) {
      right = !right;
    }

    if (right) {
      placingSoundIndex =
          Utility::wrap(placingSoundRow * ledsPerRow,
                        ((placingSoundRow * ledsPerRow) + ledsPerRow) - 1,
                        placingSoundIndex + 1);
    } else {
      placingSoundIndex =
          Utility::wrap(placingSoundRow * ledsPerRow,
                        ((placingSoundRow * ledsPerRow) + ledsPerRow) - 1,
                        placingSoundIndex - 1);
    }
  };

  void moveRow(bool down) {
    int direction = down ? 1 : -1;

    placingSoundRow = Utility::wrap(0, rows - 1, placingSoundRow + direction);

    if (down && placingSoundRow == 0 ||
        (!down) && placingSoundRow == (rows - 1)) {
      placingSoundIndex = (totalLeds - 1) - placingSoundIndex;
    } else {
      placingSoundIndex =
          Utility::reverseRange(placingSoundIndex + (ledsPerRow * direction),
                                ((placingSoundRow + 1) * ledsPerRow) - 1,
                                placingSoundRow * ledsPerRow);
    }
  };

  //__________________GETTERS AND SETTERS______________________\\

  void setLed(int index) {
    if (ledPositions[index] == 255) {
      ledPositions[index] = index;
    } else {
      ledPositions[index] = 255;
    }
  }

  void setState(State s) {
    lastState = state;
    state = s;
  }

  void setSound() {
    if (state == State::PlacingSound) {
      if (newSound) {
        newSound = false;
        Serial.write(COMMAND_NEW_SOUND);
        Serial.write(placingSoundIndex / 12);
      }

      if (ledPositions[placingSoundIndex] == 255) {
        ledPositions[placingSoundIndex] = placingSoundIndex;
      } else {
        ledPositions[placingSoundIndex] = 255;
      }
    }
  }

  State getState() { return state; }

  void changeSpeed(bool increase) {
    if (increase) {
      playbackSpeed += 10;
    } else {
      playbackSpeed -= 10;
    }

    playbackSpeed = Utility::wrap(MAX_SPEED, MIN_SPEED, playbackSpeed);
    blinkTimer.set(playbackSpeed / 2);
  }
};

#endif
