#ifndef TIMER_H
#define TIMER_H
#include <Arduino.h>

class Timer {
 private:
  unsigned long startTime;
  unsigned long finishTime;

 public:
  Timer(int finishTime) { this->finishTime = finishTime; };

  void set(int finishTime) { this->finishTime = finishTime; }
  void reset() { startTime = millis(); }
  bool isFinished() { return (millis() - startTime >= finishTime); }
};

#endif
